<?php
return array(
    'router' => array(
        'routes' => array(
            'installation.rest.installation' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/installation[/:installation_id]',
                    'defaults' => array(
                        'controller' => 'Installation\\V1\\Rest\\Installation\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'installation.rest.installation',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Installation\\V1\\Rest\\Installation\\InstallationResource' => 'Installation\\V1\\Rest\\Installation\\InstallationResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'Installation\\V1\\Rest\\Installation\\Controller' => array(
            'listener' => 'Installation\\V1\\Rest\\Installation\\InstallationResource',
            'route_name' => 'installation.rest.installation',
            'route_identifier_name' => 'installation_id',
            'collection_name' => 'installation',
            'entity_http_methods' => array(
                0 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Cms\\ExtensionManager\\Extension\\Responder',
            'collection_class' => 'Cms\\ExtensionManager\\Extension\\ResponderCollection',
            'service_name' => 'Installation',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Installation\\V1\\Rest\\Installation\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Installation\\V1\\Rest\\Installation\\Controller' => array(
                0 => 'application/vnd.installation.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Installation\\V1\\Rest\\Installation\\Controller' => array(
                0 => 'application/vnd.installation.v1+json',
                1 => 'application/json',
                2 => 'application/x-www-form-urlencoded',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Cms\\ExtensionManager\\Extension\\Responder' => array(
                'entity_identifier_name' => 'name',
                'route_name' => 'installation.rest.installation',
                'route_identifier_name' => 'installation_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'Cms\\ExtensionManager\\Extension\\ResponderCollection' => array(
                'entity_identifier_name' => 'name',
                'route_name' => 'installation.rest.installation',
                'route_identifier_name' => 'installation_id',
                'is_collection' => true,
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(),
    ),
    'zf-rpc' => array(),
    'zf-content-validation' => array(
        'Installation\\V1\\Rest\\Installation\\Controller' => array(
            'input_filter' => 'Installation\\V1\\Rest\\Installation\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Installation\\V1\\Rest\\Installation\\Validator' => array(),
    ),
);
