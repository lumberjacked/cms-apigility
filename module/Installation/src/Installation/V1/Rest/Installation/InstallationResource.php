<?php
namespace Installation\V1\Rest\Installation;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Cms\ExtensionManager\Extension\Xmanager;

class InstallationResource extends AbstractResourceListener {
    
    protected $xmanager;

    public function __construct(Xmanager $xmanager) {
        $this->xmanager = $xmanager;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {
        
        if($this->xmanager->trigger('get.cms.config')->isInstalled()) {
            return new ApiProblem(401, 'Cannot install cms anymore!');
        }
        
        return $this->xmanager->trigger('installation.event', $data);
    }
}
