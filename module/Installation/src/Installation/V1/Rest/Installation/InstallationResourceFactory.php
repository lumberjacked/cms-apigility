<?php
namespace Installation\V1\Rest\Installation;

class InstallationResourceFactory
{
    public function __invoke($services) {
        
        return new InstallationResource($services->get('Cms\ExtensionManager\Extension\XmanagerExtension'));
    }
}
