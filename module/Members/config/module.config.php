<?php
return array(
    'router' => array(
        'routes' => array(
            'members.rest.members' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/members[/:id[/:param]]',
                    'defaults' => array(
                        'controller' => 'Members\\V1\\Rest\\Members\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'members.rest.members',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Members\\V1\\Rest\\Members\\MembersResource' => 'Members\\V1\\Rest\\Members\\MembersResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'Members\\V1\\Rest\\Members\\Controller' => array(
            'listener' => 'Members\\V1\\Rest\\Members\\MembersResource',
            'route_name' => 'members.rest.members',
            'route_identifier_name' => 'id',
            'collection_name' => 'members',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Cms\\ExtensionManager\\Extension\\Responder',
            'collection_class' => 'Cms\\ExtensionManager\\Extension\\ResponderCollection',
            'service_name' => 'Members',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Members\\V1\\Rest\\Members\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Members\\V1\\Rest\\Members\\Controller' => array(
                0 => 'application/vnd.members.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Members\\V1\\Rest\\Members\\Controller' => array(
                0 => 'application/vnd.members.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Cms\\ExtensionManager\\Extension\\Responder' => array(
                'entity_identifier_name' => 'resource',
                'route_name' => 'members.rest.members',
                'route_identifier_name' => 'id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'Cms\\ExtensionManager\\Extension\\ResponderCollection' => array(
                'entity_identifier_name' => 'resource',
                'route_name' => 'members.rest.members',
                'route_identifier_name' => 'id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Members\\V1\\Rest\\Members\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
);
