<?php
namespace Members\V1\Rest\Members;

class MembersResourceFactory
{
    public function __invoke($services)
    {
        return new MembersResource($services->get('Cms\ExtensionManager\Extension\XmanagerExtension'));
    }
}
