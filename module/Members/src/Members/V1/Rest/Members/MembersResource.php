<?php
namespace Members\V1\Rest\Members;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Cms\ExtensionManager\Extension\Xmanager;

class MembersResource extends AbstractResourceListener {
    
    protected $xmanager;

    public function __construct(Xmanager $xmanager) {
        $this->xmanager = $xmanager;
    }

    public function create($data) {
        
        $responder = $this->xmanager->db('createUser', 'members', $data);
        var_dump($responder);die('resource');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {
        
        $params = $this->getEvent()->getRouteMatch()->getParams();
        
        
        if($id == 'email' && array_key_exists('param', $params)) {
            
            return $this->xmanager->db('findOneBy', 'members', array('email' => $params['param']));

        } else {
            return $this->xmanager->db('findOneBy', 'members', array('id' => $id));

        }  

        return new ApiProblem(405, 'The GET method has not been defined');         
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {
        return $this->xmanager->db('findAll', 'members');
    }

}
